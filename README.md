## SAVEPATHTOOL

### Mini tool which is pasted folder into an another folder

## Use

Unzip the package, conf the settings.yml file

## Prog install

``` 
git clone

Linux: 
python3 -m venv env
source env/bin/activate
pip install -r requirements_linux.txt

Windows
python.exe -m venv env
.\bin\Scripts\activate
pip install -r requirements_windows.txt
``` 

## Build

```
Linux : 
pyinstaller --onefile main.py

Windows
pyinstaller.exe --onefile .\main.py
```
