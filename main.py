import os
import yaml
import shutil
import datetime
import tkinter as tk
from tkinter import messagebox
import unicodedata

if __name__ == '__main__':
  root = tk.Tk()
  root.withdraw()
  # load yml file
  with open('settings.yml', 'r', encoding='utf8') as file:
      data = yaml.safe_load(file)

  # for each elements of settings (yml)
  for path in data['settings']:
      source_path = unicodedata.normalize('NFC', path['source'])
      destination_path = unicodedata.normalize('NFC', path['destination'])
      
      # Check if the path exist
      if os.path.exists(source_path):
        # Check if path is file or folder
        if os.path.isdir(source_path):
          today = datetime.date.today().strftime("%Y-%m-%d")
          # past folder
          destination_folder = os.path.join(destination_path, today, os.path.basename(source_path))
          is_source_exist = os.path.exists(destination_folder) and os.path.isdir(destination_folder)
          
          if is_source_exist:
            response = messagebox.askyesno("Confirmation", "Voulez-vous supprimer le dossier source ?")
            if response == True:
              shutil.rmtree(destination_folder)
              shutil.copytree(source_path, destination_folder)
              messagebox.showinfo("SavePathTool", f"Le dossier {destination_folder} a été remplacé avec succes !")
          else:
            shutil.copytree(source_path, destination_folder)
            messagebox.showinfo("SavePathTool", f"Le dossier {destination_folder} a été copié avec succes !")

        else:
          messagebox.showerror("SavePathTool", f"{source_path} n'est pas un dossier.")
      else:
          messagebox.showerror("SavePathTool", f"{source_path} n'existe pas.")
